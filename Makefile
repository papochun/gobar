all: install

install: gobar.go
	go install .
	cp "${PWD}/gobar-dwm" -f "${HOME}/.local/bin/"
	cp "${PWD}/gobar-spectrwm" -f "${HOME}/.local/bin/"
	chmod +x "${HOME}/.local/bin/gobar-dwm"
	chmod +x "${HOME}/.local/bin/gobar-spectrwm"
