package main

import (
	"fmt"
	"os"
	"syscall"
	"time"
	"bufio"
	"log"
	"strings"
	"strconv"
)

func wordFromFile(filename string, line int, word int) (string) {
	file, err := os.Open(filename)	
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for i := 0; i < line; i++ {
		scanner.Scan()
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	result := strings.Fields(scanner.Text())
	return result[word-1]
}

func battery() (string) {
	dir_path := "/sys/class/power_supply"
	var battery string

	dir, err := os.Open(dir_path)
	if err != nil {
		log.Fatal(err)
	}
	defer dir.Close()

	files, err := dir.Readdirnames(0)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		capacity := fmt.Sprintf("%s/%v/%s", dir_path, file, "capacity")
		if _, err := os.Stat(capacity); err != nil {
			continue
		}
		if len(battery) > 0 {
			battery += " "
		}
		battery += wordFromFile(capacity, 1, 1) + "%"
	}
	if len(battery) == 0 {
		battery = "N/A"
	}
	return battery
}

const (
   B  = 1
   KB = 1024 * B
   MB = 1024 * KB
   GB = 1024 * MB
)

func diskUsed() (uint64) {
	path := "/"
	file_system := syscall.Statfs_t{}
	err := syscall.Statfs(path, &file_system)
	if err != nil {
		log.Fatal(err)
	}

	all := file_system.Blocks * uint64(file_system.Bsize)
	free := file_system.Bfree * uint64(file_system.Bsize)
	used := all - free
	return used / GB
}

func getIdleTotal() (int, int) {
	file, err := os.Open("/proc/stat")	
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	scanner.Scan()

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	line := strings.Fields(scanner.Text())

	var total int
	for i, part_string := range line {
		// first element is "cpu" which is not a number
		if i == 0 {
			continue
		}

		part, err := strconv.Atoi(part_string)
		if err != nil {
			log.Fatal(err)
		}

		total += part
	}

	idle, err := strconv.Atoi(line[4])

	if err != nil {
        	log.Fatal(err)
    	}

	return idle, total
}

func cpu() (int) {
	idle0, total0 := getIdleTotal()
	time.Sleep(time.Second)
	idle1, total1 := getIdleTotal()

	idle := float64(idle1 - idle0)
	total := float64(total1 - total0)
	full := float64(100)

	usage := int(full * (total - idle) / total)
	return usage
}

func memory() (int) {
	file := "/proc/meminfo"

	total, err := strconv.Atoi(wordFromFile(file, 1, 2))
	free, err := strconv.Atoi(wordFromFile(file, 2, 2))
	buffer, err := strconv.Atoi(wordFromFile(file, 4, 2))
	cached, err := strconv.Atoi(wordFromFile(file, 5, 2))
	slab, err := strconv.Atoi(wordFromFile(file, 24, 2))

	if err != nil {
        	log.Fatal(err)
    	}

	result := (total - free - buffer - cached - slab) / 1000
	return result
}

func dateTime() (string, string) {
	time_full := time.Now()
	date_format := time_full.Format("Mon 02 Jan")
	time_format := time_full.Format("15:04:05")
	return date_format, time_format
}

func main() {
	date_format, time_format := dateTime()

	var status string
	status += fmt.Sprintf("  󱊣 %s", battery())
	status += fmt.Sprintf("   %d%%", cpu())
	status += fmt.Sprintf("   %dMB", memory())
	status += fmt.Sprintf("  󰆓 %dGB", diskUsed())
	status += fmt.Sprintf("  󰃭 %s", date_format)
	status += fmt.Sprintf("   %s", time_format)
	fmt.Printf("%s\n", status)
}
